<?php
$sapi_type = php_sapi_name();
if (substr($sapi_type, 0, 3) != 'cli') {
    echo 'access denied';
    die();
}
$_SERVER['DOCUMENT_ROOT'] = realpath(__DIR__ . '/../../..');
$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS',true);
define('BX_CRONTAB', true);
define('BX_NO_ACCELERATOR_RESET', true);
define("CACHED_b_iblock_type", false);
require($DOCUMENT_ROOT .'/bitrix/modules/main/include/prolog_before.php');
while (ob_get_level()) {
    ob_end_flush();
}
\Bitrix\Main\Loader::includeModule('iblock');
echo "\r\n";
$resProp = \CSaleOrderProps::GetList(
    array(),
    array(
        'CODE' => array('CORPUS'),
    )
);
if ($resProp->SelectedRowsCount() > 0) {
    echo "Свойство заказа уже существет.\r\n";
}
else {
    $arFields = array(
        "PERSON_TYPE_ID" => 1,
        "NAME" => "Корпус",
        "TYPE" => "TEXT",
        "REQUIED" => "N",
        "DEFAULT_VALUE" => "",
        "SORT" => 100,
        "CODE" => "CORPUS",
        "USER_PROPS" => "N",
        "IS_LOCATION" => "N",
        "IS_LOCATION4TAX" => "N",
        "PROPS_GROUP_ID" => 1,
        "DESCRIPTION" => "",
        "IS_EMAIL" => "N",
        "IS_PROFILE_NAME" => "N",
        "IS_PAYER" => "N",
        "IS_FILTERED" => "N",
    );
    if (\CSaleOrderProps::Add($arFields)) {
        echo "Свойство успешно добавлено.\r\n";
    } else {
        echo "Свойсво не добавлено.\r\n";
    }
}