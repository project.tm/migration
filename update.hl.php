<?php

use Bitrix\Main\Loader,
    Bitrix\Iblock\ElementTable,
    Bitrix\Highloadblock\HighloadBlockTable,
    Tools\Constants,
    Local\Model\PartnerTable,
    Local\Model\CardTable,
    Local\Highload;

// если скрипт запущен не из под консоли, запрещаем доступ
$sapi_type = php_sapi_name();

if (substr($sapi_type, 0, 3) != 'cli') {
    echo 'access denied';
    die();
}

$_SERVER['DOCUMENT_ROOT'] = realpath(__DIR__ . '/../..');
$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('BX_CRONTAB', true);
define('BX_NO_ACCELERATOR_RESET', true);

require_once($DOCUMENT_ROOT . '/bitrix/modules/main/include/prolog_before.php');

function addHighload($arFields) {
    $rsData = HighloadBlockTable::getList(array('filter' => array('NAME' => $arFields['NAME'])));
    if ($arData = $rsData->Fetch()) {
        $highLoadBlockId = $arData['ID'];
    } else {
        $arData = array('NAME' => $arFields['NAME'], 'TABLE_NAME' => $arFields['TABLE_NAME']);
        $result = HighloadBlockTable::add($arData);
        if (!$arData['ID'] = $result->getId()) {
            throw new Exception('Ошибка создания таблицы ' . $arFields['NAME']);
        }
    }
    return $arData;
}

function addFileds($arFields) {
    $obEnum = new CUserTypeEntity;
    $arFields['LIST_FILTER_LABEL'] = $arFields['LIST_COLUMN_LABEL'] = $arFields['EDIT_FORM_LABEL'];
    $rsData = CUserTypeEntity::GetList(array(), array('ENTITY_ID' => $arFields['ENTITY_ID'], 'FIELD_NAME' => $arFields['FIELD_NAME']));
    if ($arFiled = $rsData->Fetch()) {
        $obEnum->Update($arFiled['ID'], $arFields);
    } else {
        if (!$arFiled['ID'] = $obEnum->Add($arFields)) {
            throw new Exception($obEnum->LAST_ERROR);
        }
    }
    return $arFiled;
}

function addProperty($arFields) {
    $ibp = new CIBlockProperty;
    $properties = CIBlockProperty::GetList(Array(), Array("CODE" => $arFields['CODE'], "IBLOCK_ID" => $arFields['IBLOCK_ID']));
    if ($prop_fields = $properties->Fetch()) {
        $ibp->Update($prop_fields['ID'], $arFields);
    } else {
        if (!$PropID = $ibp->Add($arFields)) {
            throw new Exception($ibp->LAST_ERROR);
        }
    }
}

$error = array();
if (Loader::includeModule('iblock') and Loader::includeModule('highloadblock')) {

    /* partnet */
    $arPartnerHl = $result = addHighload(array(
        'NAME' => 'Partner', //должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
        'TABLE_NAME' => 'partner', //должно состоять только из строчных латинских букв, цифр и знака подчеркивания
    ));

    $sort = 100;
    addFileds(array(
        'ENTITY_ID' => 'HLBLOCK_' . $arPartnerHl['ID'],
        'FIELD_NAME' => 'UF_ID',
        'USER_TYPE_ID' => 'integer',
        'XML_ID' => 'UF_ID',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'OLD ID',
            'en' => 'OLD ID',
        ),
    ));
    addFileds(array(
        'ENTITY_ID' => 'HLBLOCK_' . $arPartnerHl['ID'],
        'FIELD_NAME' => 'UF_DATE_CREATE',
        'USER_TYPE_ID' => 'string',
        'XML_ID' => 'UF_DATE_CREATE',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'Дата создания',
            'en' => 'Дата создания',
        ),
    ));
    addFileds(array(
        'ENTITY_ID' => 'HLBLOCK_' . $arPartnerHl['ID'],
        'FIELD_NAME' => 'UF_ACTIVE',
        'USER_TYPE_ID' => 'boolean',
        'XML_ID' => 'UF_ACTIVE',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'Активность',
            'en' => 'Активность',
        ),
    ));
    addFileds(array(
        'ENTITY_ID' => 'HLBLOCK_' . $arPartnerHl['ID'],
        'FIELD_NAME' => 'UF_SORT',
        'USER_TYPE_ID' => 'integer',
        'XML_ID' => 'UF_SORT',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'Сортировка',
            'en' => 'Сортировка',
        ),
    ));
    $ufName = addFileds(array(
        'ENTITY_ID' => 'HLBLOCK_' . $arPartnerHl['ID'],
        'FIELD_NAME' => 'UF_NAME',
        'USER_TYPE_ID' => 'string',
        'XML_ID' => 'UF_NAME',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'Название',
            'en' => 'Название',
        ),
    ));
    addFileds(array(
        'ENTITY_ID' => 'HLBLOCK_' . $arPartnerHl['ID'],
        'FIELD_NAME' => 'UF_EMAIL',
        'USER_TYPE_ID' => 'string',
        'XML_ID' => 'UF_EMAIL',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'E-mail для оповещения',
            'en' => 'E-mail для оповещения',
        ),
    ));
    addFileds(array(
        'ENTITY_ID' => 'HLBLOCK_' . $arPartnerHl['ID'],
        'FIELD_NAME' => 'UF_IS_CARD_LOYALTY',
        'USER_TYPE_ID' => 'boolean',
        'XML_ID' => 'UF_IS_CARD_LOYALTY',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'Регистрация: Показывать "№ карты лояльности МЕТРО"',
            'en' => 'Регистрация: Показывать "№ карты лояльности МЕТРО"',
        ),
    ));
    addFileds(array(
        'ENTITY_ID' => 'HLBLOCK_' . $arPartnerHl['ID'],
        'FIELD_NAME' => 'UF_REGISTER_SMALL',
        'USER_TYPE_ID' => 'boolean',
        'XML_ID' => 'UF_REGISTER_SMALL',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'Упрощенная регистрация',
            'en' => 'Упрощенная регистрация',
        ),
    ));

//    $res = ElementTable::GetList([
//                'select' => array('*', 'PROPERY_EMAIL'),
//                'filter' => array('IBLOCK_ID' => Constants::$IBLOCK_PARTNERS)
////        'select1'=> array('ID', 'ACTIVE', 'NAME', 'PROPERTY_EMAIL', 'PROPERTY_IS_CARD_LOYALTY', 'PROPERTY_REGISTER_SMALL'),
//    ]);
    $partRes = CIBlockElement::GetList([], ['IBLOCK_ID' => Constants::$IBLOCK_PARTNERS], false, false, ['ID', 'DATE_CREATE', 'ACTIVE', 'SORT', 'NAME', 'PROPERTY_E_MAIL', 'PROPERTY_IS_CARD_LOYALTY', 'PROPERTY_REGISTER_SMALL']);
    while ($arItem = $partRes->Fetch()) {
        $arFields = array(
            'UF_ID' => $arItem['ID'],
            'UF_ACTIVE' => $arItem['ACTIVE'] == 'Y',
            'UF_DATE_CREATE' => $arItem['DATE_CREATE'],
            'UF_NAME' => $arItem['NAME'],
            'UF_SORT' => $arItem['SORT'],
            'UF_EMAIL' => $arItem['PROPERTY_E_MAIL_VALUE'],
            'UF_IS_CARD_LOYALTY' => $arItem['PROPERTY_IS_CARD_LOYALTY_VALUE'] == 'Y',
            'UF_REGISTER_SMALL' => $arItem['PROPERTY_REGISTER_SMALL_VALUE'] == 'Y',
        );
        $arPartner = PartnerTable::GetList([
                    'select' => ['ID'],
                    'filter' => ['UF_ID' => $arItem['ID']],
                ])->Fetch();
        if ($arPartner) {
            $res = PartnerTable::update($arPartner['ID'], $arFields);
            if (!$res->isSuccess()) {
                throw new \Exception($res->getErrorMessages());
            }
        } else {
            $res = PartnerTable::add($arFields);
            if (!$res->isSuccess()) {
                throw new \Exception($res->getErrorMessages());
            }
        }
    }


    /* user */
    $sort = 200;
    addFileds(array(
        'ENTITY_ID' => 'USER',
        'FIELD_NAME' => 'UF_PARTNER_HL',
        'USER_TYPE_ID' => 'hlblock',
        'XML_ID' => 'UF_PARTNER_HL',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
            'HLBLOCK_ID' => $arPartnerHl['ID'],
            'HLFIELD_ID' => $ufName['ID'],
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'Партнет HL',
            'en' => 'Партнет HL',
        ),
    ));

    $res = CUser::GetList(($by = "ID"), ($order = "ASC"), array('UF_PARTNER_1HL' => false, 'ID' => '420'), array("SELECT" => array("ID", 'NAME', 'UF_PARTNER')));
    while ($arUser = $res->Fetch()) {
        if (!empty($arUser['UF_PARTNER'])) {
            $arPartner = PartnerTable::GetList([
                        'select' => ['ID'],
                        'filter' => ['UF_ID' => $arUser['UF_PARTNER']],
                    ])->Fetch();
            $user = new CUser;
            $fields = Array(
                "UF_PARTNER_HL" => $arPartner['ID'],
            );
            $user->Update($arUser['ID'], $fields);
        }
    }


    /* card */
    $arCard = $result = addHighload(array(
        'NAME' => 'Card', //должно начинаться с заглавной буквы и состоять только из латинских букв и цифр
        'TABLE_NAME' => 'card', //должно состоять только из строчных латинских букв, цифр и знака подчеркивания
    ));

    $sort = 100;
    addFileds(array(
        'ENTITY_ID' => 'HLBLOCK_' . $arCard['ID'],
        'FIELD_NAME' => 'UF_ID',
        'USER_TYPE_ID' => 'integer',
        'XML_ID' => 'UF_ID',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'OLD ID',
            'en' => 'OLD ID',
        ),
    ));
    addFileds(array(
        'ENTITY_ID' => 'HLBLOCK_' . $arCard['ID'],
        'FIELD_NAME' => 'UF_ACTIVE',
        'USER_TYPE_ID' => 'boolean',
        'XML_ID' => 'UF_ACTIVE',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'Активность',
            'en' => 'Активность',
        ),
    ));
//    addFileds(array(
//        'ENTITY_ID' => 'HLBLOCK_' . $arCard['ID'],
//        'FIELD_NAME' => 'UF_SORT',
//        'USER_TYPE_ID' => 'integer',
//        'XML_ID' => 'UF_SORT',
//        'SORT' => $sort += 100,
//        'MULTIPLE' => NULL,
//        'MANDATORY' => NULL,
//        'SHOW_FILTER' => 'I',
//        'SHOW_IN_LIST' => NULL,
//        'EDIT_IN_LIST' => NULL,
//        'IS_SEARCHABLE' => NULL,
//        'SETTINGS' =>
//        array(
//            'DEFAULT_VALUE' => '',
//            'SIZE' => '20',
//            'ROWS' => '1',
//            'MIN_LENGTH' => '0',
//            'MAX_LENGTH' => '0',
//            'REGEXP' => '',
//        ),
//        'EDIT_FORM_LABEL' =>
//        array(
//            'ru' => 'Сортировка',
//            'en' => 'Сортировка',
//        ),
//    ));
    $ufName = addFileds(array(
        'ENTITY_ID' => 'HLBLOCK_' . $arCard['ID'],
        'FIELD_NAME' => 'UF_NAME',
        'USER_TYPE_ID' => 'string',
        'XML_ID' => 'UF_NAME',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'Название',
            'en' => 'Название',
        ),
    ));

    addFileds(array(
        'ENTITY_ID' => 'HLBLOCK_' . $arCard['ID'],
        'FIELD_NAME' => 'UF_PARTNER',
        'USER_TYPE_ID' => 'string',
        'XML_ID' => 'UF_PARTNER',
        'SORT' => $sort += 100,
        'MULTIPLE' => NULL,
        'MANDATORY' => NULL,
        'SHOW_FILTER' => 'I',
        'SHOW_IN_LIST' => NULL,
        'EDIT_IN_LIST' => NULL,
        'IS_SEARCHABLE' => NULL,
        'SETTINGS' =>
        array(
            'DEFAULT_VALUE' => '',
            'SIZE' => '20',
            'ROWS' => '1',
            'MIN_LENGTH' => '0',
            'MAX_LENGTH' => '0',
            'REGEXP' => '',
            'HLBLOCK_ID' => $arPartner['ID'],
            'HLFIELD_ID' => $ufName['ID'],
        ),
        'EDIT_FORM_LABEL' =>
        array(
            'ru' => 'Партнер',
            'en' => 'Партнер',
        ),
    ));

    $partRes = CIBlockElement::GetList(['ID' => 'DESC'], ['IBLOCK_ID' => Constants::$IBLOCK_CARDS, 'PROPERTY_PARTNER' => 457], false, false, ['ID', 'DATE_CREATE', 'ACTIVE', 'SORT', 'NAME', 'PROPERTY_PARTNER']);
    while ($arItem = $partRes->Fetch()) {
        if ($arItem['PROPERTY_PARTNER_VALUE']) {
            $arPartner = PartnerTable::GetList([
                        'select' => ['ID'],
                        'filter' => ['UF_ID' => $arItem['PROPERTY_PARTNER_VALUE']],
                    ])->Fetch();
        } else {
            $arPartner['ID'] = 0;
        }
        $arFields = array(
            'UF_ID' => $arItem['ID'],
            'UF_ACTIVE' => $arItem['ACTIVE'] == 'Y',
            'UF_DATE_CREATE' => $arItem['DATE_CREATE'],
            'UF_NAME' => $arItem['NAME'],
//            'UF_SORT' => $arItem['SORT'],
            'UF_PARTNER' => $arPartner['ID']
        );
        $arCard = CardTable::GetList([
                    'select' => ['ID'],
                    'filter' => ['UF_NAME' => $arItem['NAME']],
                ])->Fetch();
        if ($arCard) {
            $res = CardTable::update($arCard['ID'], $arFields);
            if (!$res->isSuccess()) {
                throw new \Exception($res->getErrorMessages());
            }
        } else {
            $res = CardTable::add($arFields);
            if (!$res->isSuccess()) {
                throw new \Exception($res->getErrorMessages());
            }
        }
    }


    addProperty(Array(
        "NAME" => "Партнер HL",
        "ACTIVE" => "Y",
        "SORT" => "500",
        "CODE" => "PARTNER_HL",
        "PROPERTY_TYPE" => "S",
        "USER_TYPE" => "directory",
        "USER_TYPE_SETTINGS" => array(
            'TABLE_NAME' => 'partner'
        ),
        "IBLOCK_ID" => \Tools\Constants::$IBLOCK_APPLICATIONS
    ));
    addProperty(Array(
        "NAME" => "Карта HL",
        "ACTIVE" => "Y",
        "SORT" => "500",
        "CODE" => "CARD_HL",
        "PROPERTY_TYPE" => "S",
        "USER_TYPE" => "directory",
        "USER_TYPE_SETTINGS" => array(
            'TABLE_NAME' => 'card'
        ),
        "IBLOCK_ID" => \Tools\Constants::$IBLOCK_APPLICATIONS
    ));

    $Res = CIBlockElement::GetList([], ['IBLOCK_ID' => Constants::$IBLOCK_APPLICATIONS], false, false, ['ID', 'PROPERTY_PARTNER', 'PROPERTY_CARD.NAME']);
    while ($arItem = $Res->Fetch()) {
        $propFields = array(
        );
        if ($arItem['PROPERTY_PARTNER_VALUE']) {
            $propFields['PARTNER_HL'] = PartnerTable::GetList([
                        'select' => ['ID'],
                        'filter' => ['UF_ID' => $arItem['PROPERTY_PARTNER_VALUE']],
                    ])->Fetch()['ID'];
        }
        if ($arItem['PROPERTY_PARTNER_VALUE']) {
            $propFields['CARD_HL'] = CardTable::GetList([
                        'select' => ['ID'],
                        'filter' => ['UF_NAME' => $arItem['PROPERTY_CARD_NAME']],
                    ])->Fetch()['ID'];
        }
        pre($arItem, $propFields);
        foreach ($propFields as $key => $value) {
            CIBlockElement::SetPropertyValues($arItem['ID'], Constants::$IBLOCK_APPLICATIONS, $value, $key);
        }
        break;
    }
}
echo implode(PHP_EOL, $errors) . PHP_EOL;
