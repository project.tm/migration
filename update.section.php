<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");


$bs = new CIBlockSection;
if (CModule::IncludeModule('project.core')) {
    $arFilter = Array('IBLOCK_ID' => Project\Core\Iblock\Config::IBLOCK_ID);
    $res = CIBlockSection::GetList(array(), $arFilter, false, array('ID', 'NAME', 'CODE', 'UF_CODE'));
    while ($arItem = $res->Fetch()) {
        if (empty($arItem['UF_CODE'])) {
            $arItem['UF_CODE'] = $arItem['CODE'];
            Project\Core\Redirect::add('/catalog/' . $arItem['CODE'], 'SECTION', $arItem['ID']);
            $bs->Update($arItem['ID'], array(
                'CODE' => strtolower($arItem['CODE'])
            ));
        }
        pre($arItem);
    }
}