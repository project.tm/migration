<?

use Bitrix\Main\Loader,
    Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

if (Loader::includeModule('import.catalog')) {
    $request = Application::getInstance()->getContext()->getRequest();
    $page1 = $request->get('page1') ?: 1;
    $page = $request->get('page') ?: 1;

    $connection = \Bitrix\Main\Application::getConnection('old');
    $connection->queryExecute("SET NAMES 'cp1251'");
    $connection->queryExecute('SET collation_connection = "cp1251_general_ci"');


    $time = time();
    $limit = 300;
    $IBLOCK_ID = 2;
    $arFilter = Array(
        "IBLOCK_ID" => $IBLOCK_ID,
//        "ACTIVE" => 'Y',
        "!PROPERTY_ARTNUMBER" => false,
        "PROPERTY_OLD_ID" => false,
    );
    $arSelect = array(
        "ID",
        'PROPERTY_ARTNUMBER',
        'PROPERTY_OLD_ID'
    );

    $res = CIBlockElement::GetList(array('ACTIVE_FROM' => 'nulls,asc'), $arFilter, false, Array("nPageSize" => $limit, "iNumPage" => $page), $arSelect);
//    $res = CIBlockElement::GetList(array('ACTIVE_FROM'=>'nulls,asc'), $arFilter, false, false, $arSelect);
    $arResult['PAGE_COUNT'] = $res->NavPageCount;
    $arResult['PAGE_ITEM'] = $res->NavPageNomer;
    $arResult['PAGE_IS_NEXT'] = $arResult['PAGE_ITEM'] < $arResult['PAGE_COUNT'];

    $count = $limit * $arResult['PAGE_COUNT'];
    echo '<h3>Выполнено ' . round(($page - 1) / ceil($count / $limit) * 100, 5) . '% (' . ($page - 1) * $limit . '/' . $res->SelectedRowsCount() . ')</h3>';
    $el = new CIBlockElement;
    while ($arItem = $res->Fetch()) {
        if (empty($arItem['PROPERTY_ARTNUMBER_VALUE'])) {
//            preExit($arItem);
            continue;
        }
        $oldId = (int) str_replace('WT-', '', $arItem['PROPERTY_ARTNUMBER_VALUE']);
        if ($oldId and $oldId != $arItem['PROPERTY_ARTNUMBER_VALUE']) {
            CIBlockElement::SetPropertyValues($arItem['ID'], $IBLOCK_ID, $oldId, 'OLD_ID');
//            pre($arItem['ID'], $IBLOCK_ID, $oldId, 'OLD_ID');
        } else {
            foreach (array('articul', 'articul_2', 'articul_3', 'articul_4') as $value) {
                $param = array(
                    'select' => array('id'),
                    'filter' => array(
                        $value => iconv('utf-8', 'cp1251', $arItem['PROPERTY_ARTNUMBER_VALUE']),
                    ),
                );
                $rsData = Import\Catalog\Search\Westtech\ItemTable::GetList($param);
                $rsData = new CDBResult($rsData);
                if ($row = $rsData->Fetch()) {
                    CIBlockElement::SetPropertyValues($arItem['ID'], $IBLOCK_ID, $row['id'], 'OLD_ID');
//                    pre($arItem['ID'], $IBLOCK_ID, $row['id'], 'OLD_ID');
//                    preExit($arItem);
                    break;
                }
            }
        }
//        preExit($arItem);
    }

    echo '<h4>' . (time() - $time) . 'сек </h4>';
    if ($arResult['PAGE_IS_NEXT']) {
        $param = array(
            'page1' => ++$page1,
            'page' => $page,
            'time' => time(),
        );
        echo '<META http-equiv="refresh" content="1; URL=?' . http_build_query($param) . '">';
        echo '<a href="?' . http_build_query($param) . '">next</>';
    } else {
        //    echo '<META http-equiv="refresh" content="1; URL=/import/product.clear.php">';
    }
}
