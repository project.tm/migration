<?

use Bitrix\Main\Loader,
    Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

if (Loader::includeModule('iblock') and Loader::includeModule('catalog') and Loader::includeModule('project.core')) {
    $request = Application::getInstance()->getContext()->getRequest();
    $page = $request->get('page') ?: 1;

    $time = time();
    $limit = 1000;
    $IBLOCK_ID = 2;
    $arFilter = Array(
        "IBLOCK_ID" => $IBLOCK_ID,
        "!CATALOG_PRICE_1" => false,
        "!CATALOG_PRICE_1" => '0.00'
    );
    $res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, Array("nPageSize" => $limit, "iNumPage" => $page), array("ID", 'PROPERTY_MINIMUM_PRICE', 'CATALOG_GROUP_1'));
    $arResult['PAGE_COUNT'] = $res->NavPageCount;
    $arResult['PAGE_ITEM'] = $res->NavPageNomer;
    $arResult['PAGE_IS_NEXT'] = $arResult['PAGE_ITEM'] < $arResult['PAGE_COUNT'];

    $count = $limit * $arResult['PAGE_COUNT'];
    echo '<h3>Выполнено ' . round(($page - 1) / ceil($count / $limit) * 100, 5) . '% (' . ($page-1)*$limit . '/' . $res->SelectedRowsCount() . ')</h3>';
    while ($arItem = $res->Fetch()) {
        $price = $arItem['CATALOG_PRICE_1'];
        $currency = $arItem['CATALOG_CURRENCY_1'];
        if ($currency != 'RUB') {
            $price = round(CCurrencyRates::ConvertCurrency($price, $currency, "RUB"));
        }
        $price = (int)$price;
        if ($price != $arItem['PROPERTY_MINIMUM_PRICE_VALUE']) {
            CIBlockElement::SetPropertyValues($arItem['ID'], $IBLOCK_ID, $price, 'MINIMUM_PRICE');
//            pre($arItem['ID'], $IBLOCK_ID, $price, 'MINIMUM_PRICE');
        }
    }
    echo '<h4>'. (time()-$time) . 'сек </h4>';
    if ($arResult['PAGE_IS_NEXT'] ) {
        $param = array(
            'page' => ++$page,
            'time' => time(),
        );
        echo '<META http-equiv="refresh" content="1; URL=?' . http_build_query($param) . '">';
        echo '<a href="?' . http_build_query($param) . '">next</>';
    } else {
    //    echo '<META http-equiv="refresh" content="1; URL=/import/product.clear.php">';
    }
}
