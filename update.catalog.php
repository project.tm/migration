<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
$APPLICATION->SetTitle("Обнуление остатков шин");
CModule::IncludeModule('iblock');
CModule::IncludeModule("catalog");
$IBLOCK_ID = 5;
$segodna = ConvertTimeStamp(time(), "SHORT");
?>
<?
echo "<p>Обнуление шин!</p><br>";
//echo date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), mktime(0,0,0,8,24,2014));
$arFilter = Array(
    "IBLOCK_ID" => $IBLOCK_ID,
    ">CATALOG_QUANTITY" => 0
);
$rs = CIBlockElement::GetList(
                array("timestamp_x" => "DESC"), $arFilter, false, Array("nPageSize" => 500, "iNumPage" => ceil($_REQUEST["PAGEN_1"])), array("ID")
);
$cnt = $rs->SelectedRowsCount();
echo 'Осталось не обработанными: ' . $cnt . '<br>';
if ($cnt > 0) {
    while ($ar = $rs->Fetch()) {
        $ID = $ar['ID'];
        $arCatalog = CCatalogProduct::GetByID($ID);
        if ($arCatalog) {
            $arFields = array(
                "QUANTITY" => 0
            );
            CCatalogProduct::Update($ID, $arFields);
        } else {
            $arFields = array(
                "ID" => $ID,
                "QUANTITY" => 0
            );
            CCatalogProduct::Add($arFields);
        }
        echo "Обнулили диск " . $ar['ID'] . '<br>';
    }
    echo $rs->GetPageNavStringEx($navComponentObject, "", "parser", "false");
} else {
    echo 'Нечего обновлять';
}
?>